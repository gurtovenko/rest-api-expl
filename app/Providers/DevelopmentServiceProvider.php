<?php

declare(strict_types = 1);

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Config;
use Illuminate\Support\ServiceProvider;
use Barryvdh\Debugbar\ServiceProvider as DebugBarServiceProvider;

/**
 * Class DevelopmentServiceProvider
 *
 * @package App\Providers
 */
class DevelopmentServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $providers = [
        DebugBarServiceProvider::class,
        IdeHelperServiceProvider::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        if (!Config::get('app.debug')) {
            return;
        }

        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
}
